# Copyright 2015-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="$(ever replace 2 '%2B')-0ubuntu1"

require gitlab [ user=accounts-sso suffix=tar.bz2 ] qmake [ slot=5 ]

export_exlib_phases src_prepare src_install

SUMMARY="Online Accounts Sign-on UI"
DESCRIPTION="
UI component responsible for handling the user interactions which can happen during the login
process of an online account.
"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

# require running a virtual X server (xvfb-run)
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-libs/accounts-qt[-qt4]
        net-libs/libproxy:1[>=0.4.12]
        net-libs/signon[-qt4]
        x11-libs/libnotify
        x11-libs/qtbase:5[gui]
        x11-libs/qtwebkit:5
"

EQMAKE_PARAMS+=(
    PREFIX=/usr/$(exhost --target)
)

signon-ui_src_prepare() {
    default

    edo sed -e "s:\(system(\)pkg-config:\1${PKG_CONFIG}:" \
            -e '/-Werror/d' \
            -i common-project-config.pri

    # do not install tests
    edo echo 'INSTALLS =' >>tests/unit/tst_inactivity_timer.pro
    edo echo 'INSTALLS =' >>tests/unit/tst_signon_ui.pro
}

signon-ui_src_install() {
    default

    edo mv "${IMAGE}"/usr/$(exhost --target)/share/dbus-1 "${IMAGE}"/usr/share
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
}

