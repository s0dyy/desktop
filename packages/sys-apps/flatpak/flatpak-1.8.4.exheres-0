# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ suffix=tar.xz release=${PV} ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require python [ blacklist=2 multibuild=false ]
require test-dbus-daemon bash-completion zsh-completion
require utf8-locale

SUMMARY="Linux application sandboxing and distribution framework"
HOMEPAGE="https://flatpak.org/"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    ( providers: systemd )
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2
        app-text/docbook-xsl-stylesheets
        app-text/xmlto
        dev-libs/libxslt
        dev-python/pyparsing[python_abis:*(-)?]
        sys-devel/bison
        sys-devel/gettext[>=0.18.2]
        sys-devel/libtool
        virtual/pkg-config[>=0.24]
        gtk-doc? ( dev-doc/gtk-doc[>=1.20] )
    build+run:
        group/flatpak
        user/flatpak
        app-arch/libarchive[>=2.8.0]
        app-arch/zstd[>=0.8.1]
        app-crypt/gpgme[>=1.1.8]
        core/json-glib
        dev-libs/appstream-glib[>=0.5.10]
        dev-libs/glib:2[>=2.60]
        dev-libs/libxml2:2.0[>=2.4]
        dev-libs/malcontent[>=0.4.0]
        gnome-desktop/dconf[>=0.26]
        gnome-desktop/libsoup:2.4
        sys-apps/bubblewrap[>=0.4.0]
        sys-auth/polkit:1[>=0.98]
        sys-devel/libostree:1[>=2018.9]
        sys-fs/fuse:0[>=2.9.2]
        sys-libs/libcap
        sys-libs/libseccomp
        x11-libs/gdk-pixbuf:2.0
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.40.0] )
        providers:systemd? ( sys-apps/systemd )
    run:
        sys-apps/xdg-dbus-proxy
    recommendation:
        sys-apps/xdg-desktop-portal[>=0.10] [[ description = [ Most flatpaks require
            xdg-desktop-portal at runtime ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/musl.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-docbook-docs
    --enable-documentation
    --enable-sandboxed-triggers
    --enable-seccomp
    --enable-system-helper
    --disable-auto-sideloading
    --disable-coverage
    # Checks for /usr/share/selinux/devel/Makefile, no idea where that lives
    --disable-selinux-module
    --disable-sudo
    --disable-xauth
    # Otherwise flatpak tries to use /var/lib/lib/flatpak
    --localstatedir=/var
    --with-priv-mode=none
    --with-dbus_config_dir=/usr/share/dbus-1/system.d
    --with-system-bubblewrap
    --with-system-dbus-proxy
    --with-system-install-dir=/var/lib/flatpak
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:systemd systemd'
)

# testlibrary and test-libglnx-fdio fail due to sydbox
RESTRICT="test"

src_prepare() {
    default

    # Don't use /var/tmp as test dir, but ${TEMP}
    edo sed -e "s:/var/tmp/test-flatpak-XXXXXX:"${TEMP%/}"/test-flatpak-XXXXXX:" -i \
        tests/libtest.sh

    # We don't have /usr/lib/locale/C.UTF8
    edo sed "/lib\/locale/d" -i tests/make-test-runtime.sh

    # Python isn't discovered by the build system and isn't used to invoke
    # variant-schema-compiler directly, besides the shebang.
    edo sed "/^#!\/usr\/bin\/env /s/python3/python$(python_get_abi)/" \
        -i variant-schema-compiler/variant-schema-compiler
}

src_test() {
    # for make-test-runtime.sh
    require_utf8_locale

    esandbox allow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"

    test-dbus-daemon_src_test

    esandbox disallow_net "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${TEMP%/}/test-flatpak-*/gpghome/S.gpg-agent*"
}

src_install() {
    default

    option bash-completion || edo rm -r "${IMAGE}"/usr/share/bash-completion
    option zsh-completion || edo rm -r "${IMAGE}"/usr/share/zsh

    keepdir /var/lib/flatpak
}

