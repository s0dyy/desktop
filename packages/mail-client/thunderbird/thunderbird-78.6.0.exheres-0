# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Copyright 2015 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MOZ_CODENAME="Daily"

# autoconf:2.5 doesn't work - https://bugzilla.mozilla.org/show_bug.cgi?id=104642
require autotools [ supported_autoconf=[ 2.1 ] supported_automake=[ 1.16 1.15 1.13 1.12 ] ]
require mozilla-pgo [ co_project=comm/mail codename="${MOZ_CODENAME}" ]
# FIXME: The bindist option as handled by mozilla.exlib doesn't work with current
# Thunderbirds anymore. I couldn't find out how to do it these days either so for
# now it's gone.
require toolchain-funcs utf8-locale

SUMMARY="Mozilla's standalone mail and news client"
HOMEPAGE="https://www.mozilla.com/en-US/${PN}/"
DOWNLOADS="https://ftp.mozilla.org/pub/${PN}/releases/${PV}/source/${PNV}.source.tar.xz"

REMOTE_IDS="freshcode:${PN}"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}${PV}/releasenotes/"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    accessibility
    libproxy    [[ description = [ Use libproxy for system proxy settings ] ]]
    necko-wifi  [[ description = [ Scan WiFi with its internal network library ] ]]
    pulseaudio
    wayland   [[ description = [ Use wayland as the default backend ] ]]

    ( providers: jpeg-turbo )
"

DEPENDENCIES="
    build:
        app-arch/zip
        dev-lang/nasm
        dev-lang/clang:*[>=5.0]
        dev-lang/perl:*[>=5.0]
        dev-lang/python:*[>=3.5][sqlite]
        dev-lang/yasm
        dev-python/ply
        virtual/pkg-config
        virtual/unzip
    build+run:
        app-spell/hunspell:=
        dev-lang/llvm:=[>=3.9]
        dev-lang/node
        dev-lang/rust:*[>=1.41]
        dev-libs/glib:2[>=2.36]
        dev-libs/dbus-glib:1
        dev-libs/icu:=[>=63.1]
        dev-libs/libevent:=
        dev-libs/libffi:=[>=3.0.9]
        dev-libs/libIDL:2[>=0.8.0]
        dev-libs/nspr[>=4.25]
        dev-rust/cbindgen[>=0.14.1]
        media-libs/fontconfig[>=2.7.0]
        media-libs/freetype:2[>=2.1.0]
        media-libs/libvpx:=[>=1.8.0]
        sys-apps/dbus[>=0.60]
        sys-libs/zlib[>=1.2.3]
        sys-sound/alsa-lib
        x11-dri/libdrm[>=2.4]
        x11-libs/cairo[X][>=1.10]
        x11-libs/gdk-pixbuf:2.0[>=2.26.5][X]
        x11-libs/gtk+:2[>=2.18.0]
        x11-libs/gtk+:3[>=3.22.0][wayland]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXext
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXt
        x11-libs/pango[>=1.22][X(+)]
        x11-libs/pixman:1[>=0.19.2]
        libproxy? ( net-libs/libproxy:1 )
        necko-wifi? ( net-wireless/wireless_tools )
        !pgo? ( dev-libs/nss[>=3.53.1] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        pulseaudio? ( media-sound/pulseaudio )
    suggestion:
        x11-libs/libnotify[>=0.4] [[ description = [ Show notifications with libnotify ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/bundled-botan-cc-fix.patch
    "${FILES}"/632353012d05.patch
)

MOZILLA_SRC_CONFIGURE_PARAMS=(
    --disable-install-strip
    --disable-strip
    # webrtc fails to build, missing pk11pub.h
    --disable-webrtc
    # HTML5 media support
    --enable-alsa
    --enable-dbus
    --enable-rust-simd
    --enable-webspeech

    --enable-system-ffi
    --with-system-icu
    --with-system-libevent
    --with-system-libvpx
    #--with-system-png # needs libpng with APNG
    --with-system-zlib

    # v requires sqlite built with -DSQLITE_SECURE_DELETE
    # v and enabling it resulted in linker issues
    #--enable-system-sqlite

    # optimizations
    --disable-crashreporter
    --disable-updater

    --enable-calendar           # (lightning calendar)
    --enable-hardening
    --enable-sandbox
)

MOZILLA_SRC_CONFIGURE_OPTIONS=(
    # pgo fails to build with system nss: "x86_64-pc-linux-gnu-ld: error:
    # cannot find -lnssutil3"
    'debug --without-system-nss'
    'pgo --without-system-nss'
)

MOZILLA_SRC_CONFIGURE_OPTION_ENABLES=(
    accessibility
    libproxy
    necko-wifi
    pulseaudio
)
MOZILLA_SRC_CONFIGURE_OPTION_WITHS=(
    # --with-system-jpeg needs libjpeg-turbo or whatever provides JCS_EXTENSIONS
    'providers:jpeg-turbo system-jpeg'
)

pkg_pretend() {
    if has_version ${CATEGORY}/${PN}[bindist]; then
        ewarn "The bindist option had to be hard-disabled for ${CATEGORY}/${PNV}. Your current installation"
        ewarn "has this option enabled. You should be aware that you must not distribute builds"
        ewarn "without the bindist option. If this is a problem for you, fix the exheres."
    fi
}

src_prepare() {
    require_utf8_locale

    export TOOLCHAIN_PREFIX=$(exhost --tool-prefix)

    edo sed "s/compiler_so/'${CC}')/" -i third_party/python/psutil/setup.py

    # those tests seem to fail in non-UTC timezone
    edo rm js/src/jit-test/tests/sunspider/check-date-format-{xparb,tofte}.js

    mozilla-pgo_src_prepare

    edo sed -i -e "s:objdump:${TOOLCHAIN_PREFIX}&:" python/mozbuild/mozbuild/configure/check_debug_ranges.py

    edo sed -i -e "s:objdump:\$(TOOLCHAIN_PREFIX)&:g" Makefile.in

    eautoreconf
}

src_configure() {
    # x86_64-pc-linux-gnu-as: invalid option -- 'N'
    export AS=$(exhost --tool-prefix)cc

    if cxx-is-gcc; then
        botan_compiler=gcc
    elif cxx-is-clang; then
        botan_compiler=clang
    else
        die "Unknown compiler ${CXX}; you will need to add a check for it to the thunderbird exheres"
    fi

    esandbox allow_net --connect "unix:/run/uuidd/request"
    mozilla-pgo_src_configure \
        EXHERBO_BOTAN_CC=${botan_compiler} \
        $(cc-is-clang || echo "--with-clang-path=/usr/$(exhost --build)/bin/$(exhost --tool-prefix)clang") \
        $(ld-is-lld && echo "--disable-elf-hack") \
        --enable-default-toolkit=cairo-gtk3$(option wayland -wayland)
    esandbox disallow_net --connect "unix:/run/uuidd/request"
}

src_install() {
    mozilla-pgo_src_install
}

