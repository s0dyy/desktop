# Copyright 2015-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=accounts-sso tag=VERSION_${PV} suffix=tar.bz2 ] \
    qmake [ slot=5 ]

export_exlib_phases src_prepare src_install

SUMMARY="OAuth 1.0/2.0 plugin for the SignOn daemon"
DESCRIPTION="
This plugin for the Accounts-SSO SignOn daemon handles the OAuth 1.0 and 2.0 authentication
protocols.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-libs/signon
        x11-libs/qtbase:5
"

EQMAKE_SOURCES="signon-oauth2.pro"
EQMAKE_PARAMS+=(
    PREFIX=/usr/$(exhost --target)
    LIBDIR=/usr/$(exhost --target)/lib
)

signon-plugin-oauth2_src_prepare() {
    default

    # TODO: fix upstream
    edo sed \
        -e "s:\(system(\)pkg-config:\1${PKG_CONFIG}:" \
        -e '/-Werror/d' \
        -i common-project-config.pri

    # do not install tests
    edo echo 'INSTALLS =' >>tests/tests.pro
}

signon-plugin-oauth2_src_install() {
    default

    # provided by kde/kaccounts-providers
    edo rm "${IMAGE}"/etc/signon-ui/webkit-options.d/www.facebook.com.conf
}

