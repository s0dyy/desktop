# Copyright 2018 Thomas Anderson <tanderson@caltech.edu>
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ ] systemd_user_files=[ packaging/linux/systemd/kbfs.service ] ]

export_exlib_phases src_prepare src_compile src_install

SUMMARY="Keybase filesystem"
HOMEPAGE="https://keybase.io/"

LICENCES="BSD-3"
SLOT="0"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.7.1]
    build+run:
        sys-fs/fuse
        app-crypt/keybase
"

kbfs_src_prepare() {
    edo mkdir -p "${WORK}"/.gopath/src/github.com/keybase/
    edo ln -s "${WORK}" "${WORK}"/.gopath/src/github.com/keybase/kbfs
    edo mkdir bin
}

kbfs_src_compile() {
    export GOPATH="${WORK}"/.gopath

    edo go build \
        -o "${WORK}"/bin/kbfsfuse \
        -tags production \
        github.com/keybase/kbfs/kbfsfuse
}

kbfs_src_install() {
    dobin bin/*
    install_systemd_files
}

