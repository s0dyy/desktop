# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=PipeWire tag=${PV} ] meson udev-rules

SUMMARY="PipeWire is a project that aims to greatly improve handling of audio and video under Linux"

HOMEPAGE="https://pipewire.org/"

LICENCES="MIT"
SLOT="0"

MYOPTIONS="
    doc
    bluetooth [[ description = [ build Pipewire's bluez plugin ] ]]
    ffmpeg [[ description = [ build Pipewire's ffmpeg plugin ] ]]
    gstreamer [[ description = [ build Pipewire's GStreamer plugins ] ]]
    jack [[ description = [ Build Pipewire's jack plugin ] ]]
    pulseaudio [[
        description = [ Build Pipewire's deprecated pulseaudio plugin ] ]]
    systemd
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-text/xmltoman
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/glib:2[>=2.32.0]
        media-libs/libsndfile[>=1.0.20]
        sys-apps/dbus
        sys-libs/vulkan-loader[>=1.1.69]
        sys-sound/alsa-lib[>=1.1.7]
        x11-libs/libX11
        bluetooth? (
            media-libs/sbc
            net-wireless/bluez
        )
        ffmpeg? ( media/ffmpeg )
        gstreamer? (
            media-libs/gstreamer:1.0[>=1.10.0]
            media-plugins/gst-plugins-base:1.0
        )
        jack? ( media-sound/jack-audio-connection-kit[>=1.9.10] )
        pulseaudio? ( media-sound/pulseaudio[>=11.1] )
        systemd? ( sys-apps/systemd )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    # pipewire-media-session is installed only when -Dexamples=true
    -Dexamples=true
    -Dman=true
    -Dinstalled_tests=false
    -Dpipewire-alsa=true
    -Dspa-plugins=true
    -Dalsa=true
    -Daudiomixer=true
    -Daudioconvert=true
    -Dbluez5-backend-native=true
    -Dbluez5-backend-ofono=true
    -Dbluez5-backend-hsphfpd=true
    -Dcontrol=true
    -Daudiotestsrc=true
    -Dsupport=true
    -Devl=false
    -Dv4l2=true
    -Dlibcamera=false
    -Dvideoconvert=true
    -Dvideotestsrc=true
    -Dvolume=true
    -Dvulkan=true
    -Dpw-cat=true
    -Dudevrulesdir=${UDEVRULESDIR}
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bluetooth bluez5'
    'doc docs'
    ffmpeg
    gstreamer
    'gstreamer gstreamer-device-provider'
    'jack jack'
    'jack pipewire-jack'
    'pulseaudio pipewire-pulseaudio'
    systemd
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

