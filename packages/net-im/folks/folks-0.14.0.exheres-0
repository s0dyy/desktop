# Copyright © 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require vala [ vala_dep=true ]
require meson

SUMMARY="People aggregation library"
DESCRIPTION="
libfolks is a library that aggregates people from multiple sources (eg,
Telepathy connection managers) to create metacontacts.
"
HOMEPAGE="https://wiki.gnome.org/Projects/Folks"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    bluetooth [[ description = [ Pull contacts from bluetooth devices ] ]]
    eds [[ description = [ Enable the evolution-data-server backend ] ]]
    import-tool [[ description = [ Enable the meta-contact import tool ] ]]
    inspect-tool [[
        description = [ Enable the data inspection tool ]
        requires = vapi
    ]]
    telepathy [[ description = [ Telepathy IM provider ] ]]
    tracker [[ description = [ Enable the tracker backend ] ]]
    vapi
    zeitgeist [[
        description = [ enable the zeitgeist (telepathy) backend ]
        requires = telepathy
    ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50.0]
        virtual/pkg-config[>=0.21]
    build+run:
        base/libgee:0.8[>=0.8.4][gobject-introspection]
        dev-libs/glib:2[>=2.44.0]
        gnome-desktop/gobject-introspection:1[>=1.30]
        bluetooth? ( gnome-desktop/evolution-data-server:1.2[>=3.33.2] )
        eds? (
            gnome-desktop/evolution-data-server:1.2[>=3.33.2][gobject-introspection][vapi]
            dev-libs/libxml2:2.0
        )
        import-tool? ( dev-libs/libxml2:2.0 )
        telepathy? (
            dev-libs/dbus-glib:1
            net-im/telepathy-glib[>=0.19.9][vapi]
        )
        tracker? ( app-pim/tracker:2.0[>=0.15.2][gobject-introspection] )
        zeitgeist? ( dev-libs/zeitgeist:2.0[>=0.9.14] )
   test:
        bluetooth? ( dev-python/python-dbusmock )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddocs=false
    -Dinstalled_tests=false
    -Dofono_backend=false
    -Dprofiling=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bluetooth bluez_backend'
    'eds eds_backend'
    'import-tool import_tool'
    'inspect-tool inspect_tool'
    'telepathy telepathy_backend'
    'tracker tracker_backend'
    'vapi vala'
    'zeitgeist'
)

