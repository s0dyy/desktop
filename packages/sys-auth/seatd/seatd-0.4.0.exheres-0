# Copyright 2020 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson [ meson_minimum_version=0.53.0 ]

SUMMARY="A seat management daemon and a universal seat management library"
HOMEPAGE="https://kl.wtf/projects/seatd/"
DOWNLOADS="https://git.sr.ht/~kennylevinsen/${PN}/archive/${PV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: elogind systemd-logind ) [[
        *description = [ Logind support ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        app-doc/scdoc[>=1.9.7]
        virtual/pkg-config
    build+run:
        providers:elogind? ( sys-auth/elogind )
        providers:systemd-logind? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dlogind=disabled
    -Dseatd=enabled
    -Dbuiltin=enabled
    -Dserver=enabled
    -Dexamples=disabled
    -Dman-pages=enabled
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:elogind -Dlogind=enabled'
    'providers:systemd-logind -Dlogind=enabled'
)

